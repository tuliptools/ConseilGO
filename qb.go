package conseilGO

import (
	bytes2 "bytes"
	"encoding/json"
	"io"
)

const CONSEIL_OP_EQ = "eq"

const ConseilSortDirection_ASC = "asc"
const ConseilSortDirection_DESC = "desc"

const ConseilOperator_BETWEEN = "between"
const ConseilOperator_EQ = "eq"
const ConseilOperator_IN = "in"
const ConseilOperator_LIKE = "like"
const ConseilOperator_LT = "lt"
const ConseilOperator_BEFORE = "before"
const ConseilOperator_GT = "gt"
const ConseilOperator_AFTER = "after"
const ConseilOperator_STARTSWITH = "startsWith"
const ConseilOperator_ENDSWITH = "endsWith"
const ConseilOperator_ISNULL = "isnull"

const ConseilFunction_avg = "avg"
const ConseilFunction_count = "count"
const ConseilFunction_max = "max"
const ConseilFunction_min = "min"
const ConseilFunction_sum = "sum"

const ConseilOutput_csv = "csv"
const ConseilOutput_json = "json"

type Predicate struct {
	Operation string   `json:"operation"`
	Values    []string `json:"set"`
	Invert    bool     `json:"inverse"`
	Field     string   `json:"field"`
}

type Ordering struct {
	Field string `json:"field"`
	Order string `json:"direction"`
}

type Aggregation struct {
}

type ConseilQueryBuilder struct {
	Fields      []string    `json:"fields"`
	Predicates  []Predicate `json:"predicates"`
	OrderBy     []Ordering  `json:"orderBy"`
	Aggregation []string    `json:"aggregation"`
	Limit       int         `json:"limit"`
}

func (this ConseilQueryBuilder) JsonReader() io.Reader {
	bytes, _ := json.Marshal(this)
	return bytes2.NewBuffer(bytes)
}

func (this *ConseilQueryBuilder) AddFields(f ...string) *ConseilQueryBuilder {

	for _, r := range f {
		this.Fields = append(this.Fields, r)
	}

	return this
}

func (this *ConseilQueryBuilder) AddPredicate(field, operation string, values []string, invert bool) *ConseilQueryBuilder {

	this.Predicates = append(this.Predicates, Predicate{
		Field:     field,
		Operation: operation,
		Values:    values,
		Invert:    invert,
	})

	return this
}

func (this *ConseilQueryBuilder) AddOrdering(field, order string) *ConseilQueryBuilder {
	this.OrderBy = append(this.OrderBy, Ordering{
		Field: field,
		Order: order,
	})
	return this
}

func (this *ConseilQueryBuilder) SetLimit(limit int) *ConseilQueryBuilder {
	this.Limit = limit
	return this
}
