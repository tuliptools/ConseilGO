package conseilGO

import (
	"io/ioutil"
	"net/http"
	"sync"
	"time"
)

type Conseil struct {
	Host      string
	Network   string
	Token     string
	Platform  string
	transport *http.Transport
}

func NewConseil(host, network, token, platform string, transport *http.Transport) *Conseil {
	c := Conseil{}
	c.Host = host
	c.Token = token
	c.Platform = platform
	c.Network = network
	if transport == nil {
		transport = &http.Transport{
			MaxIdleConns:       10,
			IdleConnTimeout:    10 * time.Second,
			DisableCompression: false,
		}
	}
	c.transport = transport

	return &c
}

type ConseilResult struct {
	Result *http.Response
	wait   *sync.WaitGroup
}

func (this *ConseilResult) Wait() {
	this.wait.Wait()
}

func (this *ConseilResult) Bytes() []byte {
	body, _ := ioutil.ReadAll(this.Result.Body)
	return body
}

func (this *ConseilResult) String() string {
	return string(this.Bytes())
}

func (this *Conseil) WaitFor(res ...*ConseilResult) {
	for _, i := range res {
		i.Wait()
	}
}

func (this *Conseil) NewQuery() *ConseilQueryBuilder {
	return &ConseilQueryBuilder{}
}

func (this *Conseil) Exec(qb *ConseilQueryBuilder, entity string) (*ConseilResult, error) {
	var url = this.Host + "/v2/data/" + this.Platform + "/" + this.Network + "/" + entity
	req, err := http.NewRequest("POST", url, qb.JsonReader())
	if err != nil {
		return nil, err
	}
	req.Header.Set("apiKey", this.Token)
	req.Header.Set("Content-Type", "application/json")

	res := ConseilResult{}
	res.wait = &sync.WaitGroup{}
	res.wait.Add(1)
	go func() {
		client := http.Client{Transport: this.transport}
		resp, _ := client.Do(req)
		res.Result = resp
		res.wait.Done()
	}()

	return &res, nil
}
