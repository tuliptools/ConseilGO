# (mini) ConseilGO

<img src="https://gitlab.com/tuliptools/ConseilGO/raw/master/gopher.png" width="500" height="500">

This is a a minimal wrapper around the conseil API, inspired by ConseilJS,
you wll find that many of the examples on the conseilJS website wil work extremely similar with conseilGO

ConseiJS docs are here: https://cryptonomic.github.io/ConseilJS

a simple go example of how to get the top 3 contracts would be:

```
package main

import (
	"fmt"
	conseilGO "gitlab.com/tuliptools/ConseilGO"
)

func main(){

	conseil := conseilGO.NewConseil(
		"https://conseil-prod.cryptonomic-infra.tech",
		"mainnet",
		"galleon",
		"tezos",
		nil,
	)

	qry := conseil.NewQuery()
	qry.AddFields("account_id", "balance")
	qry.AddPredicate("account_id", conseilGO.ConseilOperator_STARTSWITH, []string{"KT1"}, false)
	qry.AddOrdering("balance",conseilGO.ConseilSortDirection_DESC)
	qry.SetLimit(3)

	res,_ := conseil.Exec(qry,"accounts")

	// wait for the http call to complete
	// you can easily wait for multiple calls in parralell using conseil.Wait(res1,res2,res3)
	// yay concurrency
	res.Wait()

	fmt.Println(res.String())

	/*
	[
	   {
	      "account_id":"KT1F63RTxm2ztYLtikDMLK7qg62Vtj4M1trk",
	      "balance":22774502375400
	   },
	   {
	      "account_id":"KT1PuUGWvCypTrNu7yweWCzpX6zyuy6nq6Wu",
	      "balance":12575475238484
	   },
	   {
	      "account_id":"KT1Q1kfbvzteafLvnGz92DGvkdypXfTGfEA3",
	      "balance":9354941173593
	   }
	]
	*/

}
```